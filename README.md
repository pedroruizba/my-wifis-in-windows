# My wifis in windows

**Version en español**
Este programa te ayudará a ver las contraseñas guardadas en tu Windows, es un programa optimizado para Windows 10, el ejecutable .BAT puede ser modificado a tu gusto.

**English Version**
This program will help you to see the passwords saved in your Windows, it is a program optimized for Windows 10, the .BAT executable can be modified to your liking.